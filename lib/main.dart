import 'dart:io';

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  Image image = Image.network(
      'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR8lnFhISyFQKXebiuVkTRxbeC59OhxZZ2wiQ&usqp=CAU');

  Future<void> takePicture(ImageSource source) async {
    await ImagePicker().getImage(source: source).then(
          (value) => setState(() {
            if (value != null) {
              return image = Image.file(
                File(value.path),
              );
            } else {
              image = null;
            }
          }),
        );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          SizedBox(
            height: MediaQuery.of(context).size.height * 0.8,
            width: double.infinity,
            child: image != null
                ? FittedBox(
                    fit: BoxFit.cover,
                    child: image,
                  )
                : SizedBox(child: ColoredBox(color: Colors.green)),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Material(
                color: Colors.amber,
                child: InkWell(
                  onTap: () {
                    takePicture(ImageSource.gallery);
                  },
                  child: SizedBox(
                    height: 75,
                    width: 175,
                    child: Text(
                      'Image gallery',
                      textAlign: TextAlign.center,
                      style: TextStyle(color: Colors.white, fontSize: 30),
                    ),
                  ),
                ),
              ),
              Material(
                color: Colors.amber,
                child: InkWell(
                  onTap: () {
                    takePicture(ImageSource.camera);
                  },
                  child: SizedBox(
                    height: 75,
                    width: 175,
                    child: Text(
                      'Image from camera',
                      textAlign: TextAlign.center,
                      style: TextStyle(color: Colors.white, fontSize: 30),
                    ),
                  ),
                ),
              ),
            ],
          )
        ],
      ),
    );
  }
}
